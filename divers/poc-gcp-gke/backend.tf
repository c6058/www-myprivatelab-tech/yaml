terraform {
  cloud {
    organization = "organisation_terraform_cloud"
    workspaces {
      name = "workspace_terraform"
    }
  }
}