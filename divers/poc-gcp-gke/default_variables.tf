#Variables pour le module

variable "project_id" {
    type = string
    default = "vbu-test-k8s"
    description = "Identifiant du projet dans GCP"
}


variable "region"{
    type = string
    default = "europe-west1-b"
    description = "region GCP"
}

variable "credentials"{
    type = string
    default = "C:\\Temp\\poc-gcp\\k8s-service-account-keyfile.json"
    description = "Fichier JSON qui contient les elements d'authentification du compte de service"
}

variable "service_account"{
    type = string
    default = "k8s-service-account@vbu-test-k8s.iam.gserviceaccount.com"
    description = "email du compte de service"
}


#Variable du cluster

variable "preemptible"{
    type = string
    default = "true"
    description = "Indique si on utilise les noeud preemtible"
}

variable "machine_type"{
    type = string
    default = "g1-small"
    description = "type de VM pour les worker"
}

variable "node_count"{
    type = number
    default = "2"
    description = "nombre de worker"
}

variable "name_master"{
    default = "poc-k8s-master"
    description = "nom de la partie master"
}

variable "name_pool_worker"{
    default = "poc-k8s-worker"
    description = "nom de la partie worker"
}

