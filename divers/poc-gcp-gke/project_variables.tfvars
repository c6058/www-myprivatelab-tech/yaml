##Parametre GCP
project_id = "nom-projet-gcp"
region = "europe-west1-b"
credentials = "C:\\chemin\\vers\\fichier.json"
service_account = "nom-service@nom-projet-gcp.iam.gserviceaccount.com"

#Parametre du cluster
preemptible = "true"
machine_type = "g1-small"
node_count = "2"
name_master = "poc-k8s-master"
name_pool_worker = "poc-k8s-worker"