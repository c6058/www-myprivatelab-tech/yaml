resource "google_container_cluster" "primary" {
  name     = "${var.name_master}" 
  location = "${var.region}" 

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${var.name_pool_worker}" 
  location   = "${var.region}" 
  cluster    =  google_container_cluster.primary.name
  node_count = "${var.node_count}" 

  node_config {
    preemptible  = "${var.preemptible}" 
    machine_type = "${var.machine_type}" 

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = "${var.service_account}" 
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}